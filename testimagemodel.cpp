#include "testimagemodel.h"
#include <QPixmap>
#include <QImage>

/*
    Copyright (c) 2014 Sune Vuorela <sune@vuorela.dk>

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
*/
TestImageModel::TestImageModel(QObject *parent) :
    QAbstractListModel(parent)
{
    // dummy data

    QPixmap px(100,200);
    px.fill(Qt::red);
    Data red;
    red.data = px;
    red.name = QStringLiteral("Red");

    m_data << red;

    QImage im(200,50,QImage::Format_RGB32);
    im.fill(Qt::blue);
    Data blue;
    blue.data = im;
    blue.name = QStringLiteral("Blue");

    m_data << blue;

    Data pink;
    pink.data = QVariant::fromValue<QColor>(Qt::magenta);
    pink.name = QStringLiteral("Pink");

    m_data << pink;

    Data non;
    non.name = "FAIL";

    m_data << non;
}

int TestImageModel::rowCount(const QModelIndex& parent) const
{
    return m_data.size();
}

QVariant TestImageModel::data(const QModelIndex& index, int role) const
{
    if(role == TitleRole) {
        return m_data.at(index.row()).name;
    } else if(role == DataRole) {
        return m_data.at(index.row()).data;
    }
    return QVariant();
}

QHash<int, QByteArray> TestImageModel::roleNames() const
{
    QHash<int, QByteArray> roleNames;
    roleNames.insert(TitleRole,"TitleRole");
    roleNames.insert(DataRole, "DataRole");
    return roleNames;
}
