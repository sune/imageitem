/*
    Copyright (c) 2014 Sune Vuorela <sune@vuorela.dk>

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
*/
#ifndef IMAGEITEM_H
#define IMAGEITEM_H

#include <QQuickPaintedItem>

/**
 * @brief QQuickItem to show images/pixmaps/colors in a QML item
 *
 * As opposed to the Image from plain qml, this works on
 * QImages, QPixmaps and QColors
 */
class ImageItem : public QQuickPaintedItem
{
        Q_OBJECT
        Q_PROPERTY(QVariant imageData READ imageData() WRITE setImageData NOTIFY imageDataChanged)
    public:
        explicit ImageItem(QQuickItem *parent = 0);
        /**
         * \reimpl
         */
        void paint(QPainter* painter) Q_DECL_OVERRIDE;
        /**
         * @brief image data u-ed by this item
         * @return a QVariant wrapping the data
         */
        QVariant imageData() const;
        /**
         * @brief Sets the image data
         * @param newData
         */
        void setImageData(const QVariant& newData);
    Q_SIGNALS:
        /**
         * @brief imageDataChanged
         */
        void imageDataChanged();
    private:
        enum Type {
            Unknown,
            Pixmap,
            Image,
            Color
        };
        Type m_type;
        QVariant m_imageData;
        QRectF scaledRect(const QRect& sourceRect) const;

};

#endif // IMAGEITEM_H
