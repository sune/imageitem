/*
    Copyright (c) 2014 Sune Vuorela <sune@vuorela.dk>

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
*/
#include "imageitem.h"
#include <QPainter>

ImageItem::ImageItem(QQuickItem *parent) :
    QQuickPaintedItem(parent)
{
}

void ImageItem::paint(QPainter* painter)
{
    switch(m_type) {
        case Unknown: {
            return;
        }
        case Image: {
            QImage image = m_imageData.value<QImage>();
            painter->drawImage(scaledRect(image.rect()), image);
            return;
        }
        case Pixmap: {
            QPixmap pixmap = m_imageData.value<QPixmap>();
            painter->drawPixmap(scaledRect(pixmap.rect()).toRect(), pixmap);
            return;
        }
        case Color: {
            QColor color = m_imageData.value<QColor>();
            painter->fillRect(contentsBoundingRect(),color);
            return;
        }
    }
}

QVariant ImageItem::imageData() const
{
    return m_imageData;
}

void ImageItem::setImageData(const QVariant& newData)
{
    switch(newData.userType()) {
        case QMetaType::QPixmap: {
            m_type = Pixmap;
            break;
        }
        case QMetaType::QImage: {
            m_type = Image;
            break;
        }
        case QMetaType::QColor: {
            m_type = Color;
            break;
        }
        default: {
            m_type = Unknown;
            break;
        }
    }
    m_imageData = newData;
    emit imageDataChanged();
}

QRectF ImageItem::scaledRect(const QRect& sourceRect) const
{
    QRectF targetRect = contentsBoundingRect();

    QSizeF scaledSize;

    double widthScale = targetRect.width() / sourceRect.width();
    double heightScale = targetRect.height() / sourceRect.height();
    if(widthScale < heightScale) {
        scaledSize.setHeight(sourceRect.height() * widthScale);
        scaledSize.setWidth(sourceRect.width() *widthScale);
    } else {
        scaledSize.setHeight(sourceRect.height() * heightScale);
        scaledSize.setWidth(sourceRect.width() *heightScale);
    }

    QRectF result = QRectF(targetRect.left() + targetRect.width() /2 - scaledSize.width() /2,
                targetRect.top() + targetRect.height()/2 - scaledSize.height()/2,
                scaledSize.width(),scaledSize.height());
//    QRectF result(QPointF(0,0),scaledSize);
    qDebug() << result << targetRect << sourceRect << widthScale << heightScale ;
    return result;
}


