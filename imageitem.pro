TEMPLATE = app

QT += qml quick

SOURCES += main.cpp \
    testimagemodel.cpp \
    imageitem.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

HEADERS += \
    testimagemodel.h \
    imageitem.h
